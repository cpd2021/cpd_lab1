package prob2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class waitNotifSync {

    public static void run() {
        try {
            File myFile = new File("problem_text.txt");
            if(myFile.createNewFile()){
                System.out.println("File created");
            }
            else{
                System.out.println("File already exists!");
            }
            Scanner scanner = new Scanner(myFile);
            FileWriter fileWriter = new FileWriter("problem_text.txt");

            Document doc = new Document(myFile, scanner, fileWriter);
            System.out.println("Initialize threads synchronized by lock");
            Writer writer1 = new Writer(1000L, doc);
            Writer writer2 = new Writer(1000L, doc);
            Reader reader1 = new Reader(1, 1000L, doc);
            Reader reader2 = new Reader(2, 1000L, doc);
            Reader reader3 = new Reader(3, 1000L, doc);

            System.out.println("Start threads synchronized by lock");
            writer1.start();
            writer2.start();
            reader1.start();
//            reader2.start();
//            reader3.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
