package prob2;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class Document {

    File myFile;
    Scanner scanner;
    java.io.Writer writer;
    boolean writing = false;
    int readers = 0;

    public Document(File myFile, Scanner scanner, Writer writer){
        this.myFile = myFile;
        this.scanner = scanner;
        this.writer = writer;
    }

    public synchronized void write(String textToWrite){
        try{
            while(!writing){
                System.out.println("Writer waits to write");
                wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Writing \"" + textToWrite + "\" to file.");
        try {
            this.writer.write(textToWrite);
        } catch (IOException e) {
            e.printStackTrace();
        }
        writing = false;
        System.out.println("Writer notifies all threads");
        notify();
    }

    public synchronized String read(int id){
        try{
            while(writing){
                System.out.println("Reader is waiting");
                sleep(1000);
                System.out.println("Reader " + id +  " was notified.");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        StringBuilder data = new StringBuilder();
        while(scanner.hasNext()){
            data.append(scanner.nextLine());
        }
        System.out.println("Reader " + id + " is reading the file: " + data.toString());
        System.out.println("Reader " + id + " notifies threads");
        writing = true;
        notify();
        return data.toString();
    }

}
