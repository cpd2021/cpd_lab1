package prob2;

public class Writer extends Thread{
    public String[] textToWrite  = {"A","B", "C", "D", "E", "F", "G", "H", "I","J"};
    public long delay;
    private Document document;

    public Writer(long delay, Document document){
        this.delay = delay;
        this.document = document;
    }

    @Override
    public void run(){
        try{
            for(String text: textToWrite){
                sleep(delay);
                System.out.println("Writer is writing " + text);
                document.write(text);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
