package prob2;

public class Reader extends Thread{
    String text;
    long delay;
    int id;
    Document document;
    public Reader(int id, long delay, Document document){
        this.delay = delay;
        this.document = document;
        this.id = id;
    }

    @Override
    public void run(){
        try {
            while(true){
                String s = document.read(id);
                System.out.println("Reader " + this.id + " just read " + s);
                sleep(delay);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
