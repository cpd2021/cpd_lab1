package prob3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Data {
    String document;
    Queue<String> queue = new LinkedList<String>();
    boolean printing = false;

    public void produce(String document){
        System.out.println("Adding to queue: " + document);
        queue.add(document);
    }


    public String print(){
      String document = queue.poll();
        System.out.println("Printing from queue: " + document);
        return document;
    }

    public boolean hasPackets() {
        return !queue.isEmpty();
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("Queue: ");
        queue.stream().forEach(element -> builder.append(element + " "));
        return builder.toString();
    }
}
