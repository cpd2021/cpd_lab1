package prob3;

import java.util.ArrayList;

public class Clerk extends Thread{
    private int clerkId;
    private String[] documents = {"doc:a", "doc:b", "doc:c", "doc:d", "doc:e", "end"};
    public long delay;
    public Data data;

    public Clerk(Data data, int clerkId){
        this.clerkId = clerkId;
        this.data = data;
    }

    @Override
    public void run(){
        try{
            for(String document: documents){
                delay = (int) ((Math.random() * (10 - 1)) + 1);
                sleep(delay*1000);
                System.out.println("Clerk " + clerkId + " finished document " + document);
                data.produce(document);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
