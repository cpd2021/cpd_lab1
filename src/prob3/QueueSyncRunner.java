package prob3;

import java.util.ArrayList;

public final class QueueSyncRunner extends Thread {
    public static void execute() {
        Data queue = new Data();
        System.out.println("Init threads synchronized by queue");
        Printer p = new Printer(2000L, queue);

        ArrayList<Clerk> clerkList = new ArrayList<>();
        for(int i = 0; i < 8; i++){
            Clerk c = new Clerk(queue, i);
            clerkList.add(c);
        }
        p.start();
        for(Clerk c: clerkList){
            c.start();
        }
    }
}
