package prob3;

public class Printer extends Thread{
    private long delay;
    private Data data;

    public Printer(long delay, Data data){
        super();
        this.data = data;
        this.delay = delay;
    }

    @Override
    public void run(){
        try{
            while(!data.hasPackets()){
                System.out.println("Nothing to print");
                Thread.sleep(500);
            }
            while(data.hasPackets()){
                String document = data.print();
                System.out.println("Printer printing: " + document);
                Thread.sleep(delay);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
