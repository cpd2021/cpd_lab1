package queue.sync;

public class Consumer extends Thread{

    private long delay;
    private Data data;

    public Consumer(long delay, Data data){
        super();
        this.delay = delay;
        this.data= data;
    }

    @Override
    public void run(){
        try{
            while(!data.hasPackets()){
                System.out.println("Nothing to read");
                sleep(500);
            }
            while(data.hasPackets()){
                String packet = data.consume();
                System.out.println("Consumer reads: " + packet);
                sleep(delay);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
