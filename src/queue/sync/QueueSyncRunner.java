package queue.sync;

import static java.lang.Thread.sleep;

public class QueueSyncRunner {

    public static void execute(){
        Data queue = new Data();
        System.out.println("Init threads sync by queue");
        Consumer c = new Consumer(2000L, queue);
        Producer p = new Producer(1000L, queue);

//        Thread inspector = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    while(true){
//                        System.out.println(queue.toString());
//                        sleep(500);
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });

        System.out.println("Start threads sync by queue");
        p.start();
        c.start();
//        inspector.start();

    }

}
